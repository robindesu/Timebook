# Timebook

This is a Time record Web App build in React.js to controller work and lunchtime records of an employee.

## Requiriments

To execute this project in development mode it's necessary to have the latest Node.js and Yarn.

## How to install run the project

After download the font code from GitHub, install the dependencies with Yarn:

`yarn install`

To run in development mode:

`yarn start`

To test:

`yarn test`

To build the distribution App:

`yarn build`

## Store and State Management

To keep it simple was used the useContext and useReducer hooks to manage the global store to share data between the components.
As all components are functional components it's been used the useState hook for local state storage.

## Envirioment and Libs

This Application was built with the help of some libraries and tools described below:

- Create React App (https://github.com/facebook/create-react-app)

A scaffolding tool to set up a basic react environment application.

- Styled Components (https://styled-components.com/)

A tool to compile CSS elements into React Components.
It helps to scope the CSS the styled isolated into the specific component, without affecting others.

- Ant Desing (https://ant.design/)

A light and minimalistic UI library for React.

- Moment.js
  A library to handle date and time operations, it's a requirement of the data picker from the Antd.

- Eslint and Prettier

Eslint is a tool to correct the code syntax used with Prettier to format the code. The tools are configured to work with VSCode and uses the Airbnb pattern configuration;

- customize-cra and react-app-rewired

Dependencies used to customize the CRA setup withou ejecting, in this project was used to configure the root import path like '~/path' using the babel plugin 'babel-plugin-root-import';

- react-uuid
  Dependence to generate unic identifications for the data table;
