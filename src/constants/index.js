const constants = {
  DATE_FORMAT: 'DD/MM/YYYY',
  TIME_FORMAT: 'HH:mm',
  ESPECTED_TIME: '08:00',
};

export default constants;
