import firebase from 'firebase';

const firebaseConfig = {
  apiKey: 'AIzaSyDev_ItjC_5AnNqUcze2oTkdF3HG2vsPjs',
  authDomain: 'timebook-a1e7c.firebaseapp.com',
  databaseURL: 'https://timebook-a1e7c.firebaseio.com',
  projectId: 'timebook-a1e7c',
  storageBucket: 'timebook-a1e7c.appspot.com',
  messagingSenderId: '812580312548',
  appId: '1:812580312548:web:bbcb0837fb7e537effe3f1',
  measurementId: 'G-TC8T97KE3Z',
};
// Initialize Firebase
export const firebaseImpl = firebase.initializeApp(firebaseConfig);
export const firebaseDatabase = firebase.firestore();
