import React from 'react';
import { Layout } from 'antd';
import 'antd/dist/antd.css';
import MainBox from '~/components/MainBox';
import TimeForm from '~/components/TimeForm';
import TimeTable from '~/components/TimeTable';

const { Header, Content } = Layout;

function Home() {
  return (
    <Layout>
      <Header />
      <Content>
        <MainBox>
          <TimeForm />
          <TimeTable />
        </MainBox>
      </Content>
    </Layout>
  );
}

export default Home;
