import React from 'react';
import GlobalStyle from '~/styles/global';
import Home from '~/pages/Home';
import { StateProvider } from '~/store';

function App() {
  return (
    <StateProvider>
      <GlobalStyle />
      <Home />
    </StateProvider>
  );
}

export default App;
