/* eslint-disable react/jsx-filename-extension */
import React, { useContext, useState, useCallback } from 'react';
import { Form, Switch, Button, DatePicker, TimePicker, Spin } from 'antd';
import { CoffeeOutlined } from '@ant-design/icons';
import moment from 'moment';
import { FormBox, LunchBox, DateBox, InputLabel, StatusAlert } from './styles';
import { store } from '~/store';
import constants from '~/constants';
import firebaseService from '~/services/firebaseService';

const INIT_FORM_DATA = {
  init_time: moment(0, constants.TIME_FORMAT),
  end_time: moment(new Date(), constants.TIME_FORMAT),
  date: moment(new Date(), constants.DATE_FORMAT),
  amount: 0,
  isLunch: false,
};
const statusMessages = {
  success: 'Success to Save :)',
  error: 'Error to Save :(',
};

function TimeForm() {
  const [formData, setFormData] = useState(INIT_FORM_DATA);
  const [apiStatus, setApiStatus] = useState(null);
  const [loading, setLoading] = useState(false);
  const globalState = useContext(store);
  const { dispatch } = globalState;

  const handleChange = useCallback(
    (field, value) => {
      setFormData(prevState => ({
        ...prevState,
        [field]: value,
      }));
    },
    [setFormData]
  );

  const handleSubmit = useCallback(() => {
    setLoading(true);
    const item = {
      ...formData,
      init_time: formData.init_time.format(constants.TIME_FORMAT),
      end_time: formData.end_time.format(constants.TIME_FORMAT),
      amount: moment
        .utc(moment.duration(formData.end_time.diff(formData.init_time)).asMilliseconds())
        .format(constants.TIME_FORMAT),
      date: formData.date.format(constants.DATE_FORMAT),
    };
    firebaseService.sendTime(item, response => {
      if (response.id) {
        dispatch({ type: 'add_item', item });
        setApiStatus('success');
      } else {
        setApiStatus('error');
      }
      setLoading(false);
      setTimeout(() => setApiStatus(null), 3000);
    });
  }, [setLoading, dispatch, setApiStatus, formData]);

  return (
    <div>
      <FormBox name="timeform" submit={handleSubmit}>
        <Form.Item label="Arrive Time:">
          <TimePicker
            rules={[
              {
                required: true,
                message: 'Please input the time!',
              },
            ]}
            value={formData.init_time}
            format={constants.TIME_FORMAT}
            onChange={value => handleChange('init_time', value)}
          />
        </Form.Item>
        <Form.Item label="Exit Time:">
          <TimePicker
            rules={[
              {
                required: true,
                message: 'Please input the time!',
              },
            ]}
            value={formData.end_time}
            format={constants.TIME_FORMAT}
            onChange={value => handleChange('end_time', value)}
          />
        </Form.Item>
        <DateBox>
          <InputLabel>Date: </InputLabel>
          <DatePicker
            rules={[
              {
                required: true,
                message: 'Please input the DATE!',
              },
            ]}
            value={formData.date}
            format={constants.DATE_FORMAT}
            onChange={value => handleChange('date', value)}
          />
        </DateBox>
        <LunchBox>
          <InputLabel>Lunch Break? </InputLabel>
          <Switch
            checked={formData.isLunch}
            onChange={value => handleChange('isLunch', value)}
            checkedChildren={<CoffeeOutlined />}
          />
        </LunchBox>
        <Button type="primary" htmlType="submit">
          +
        </Button>
        {loading && <Spin />}
      </FormBox>
      {apiStatus && (
        <StatusAlert message={statusMessages[apiStatus]} type={apiStatus} onClick={handleSubmit} />
      )}
    </div>
  );
}

export default TimeForm;
