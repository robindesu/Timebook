import styled from 'styled-components';
import { Form as FormBase, Alert as AlertBase } from 'antd';

export const FormBox = styled(FormBase)`
  display: flex;
  justify-content: space-evenly;
  padding-top: 20px;
  flex-wrap: wrap;
`;

export const LunchBox = styled.div`
  height: 32px;
  display: flex;
  align-items: center;
`;

export const DateBox = styled.div`
  display: flex;
  align-items: baseline;
`;

export const InputLabel = styled.label`
  color: rgba(0, 0, 0, 0.85);
  font-size: 14px;
  margin-right: 7px;
`;

export const StatusAlert = styled(AlertBase)`
  width: 70%;
  text-align: center;
  margin: auto;
`;
