import styled from 'styled-components';

export default styled.div`
  background: #fff;
  margin: 30px;
  min-height: 500px;
`;
