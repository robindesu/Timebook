/* eslint-disable react/jsx-filename-extension */
import React, { useState, useEffect, useContext, useCallback } from 'react';
import { Table } from 'antd';
import uuid from 'react-uuid';
import moment from 'moment';
import { store } from '~/store';
import constants from '~/constants';
import { TableContainer } from './styles';
import { columns } from './columns';
import ExpandedRowList from './ExpandedRowList';
import FirebaseService from '~/services/firebaseService';

function TimeTable() {
  const [data, setData] = useState([]);
  const [timeHashList, setTimeHashList] = useState({});
  const globalState = useContext(store);
  const { state } = globalState;

  const timeSum = (a, b) => {
    const sum = moment
      .utc(moment.duration(a).add(moment.duration(b)).asMilliseconds())
      .format(constants.TIME_FORMAT);
    return sum;
  };

  const updateTimeList = (index, newItem) => {
    const newTimelist = { ...timeHashList };
    if (index in timeHashList) {
      newTimelist[index].push(newItem);
    } else {
      newTimelist[index] = [newItem];
    }
    setTimeHashList(newTimelist);
  };

  const handleNewItem = storedItem => {
    const newData = [...data];
    let newItem;
    const foundItemIndex = data.findIndex(item => item.date === storedItem.date);
    if (foundItemIndex > -1) {
      newItem = {
        ...data[foundItemIndex],
        end_time: storedItem.end_time,
      };
    } else {
      newItem = {
        ...storedItem,
        key: uuid(),
        lunch_amount: '00:00',
        amount: '00:00',
      };
    }
    if (storedItem.isLunch) {
      newItem = {
        ...newItem,
        lunch_amount: timeSum(newItem.lunch_amount, storedItem.amount),
      };
    } else {
      newItem = {
        ...newItem,
        amount: timeSum(newItem.amount, storedItem.amount),
      };
    }
    if (foundItemIndex > -1) {
      newData[foundItemIndex] = newItem;
    } else {
      newData.push(newItem);
    }
    setData(newData);
    updateTimeList(newItem.key, storedItem);
  };

  useEffect(() => {
    FirebaseService.getData('timebook', dataReceived => {
      dataReceived.forEach(element => {
        handleNewItem(element);
      });
    });
  }, []);

  useEffect(() => {
    if (state) {
      handleNewItem(state);
    }
  }, [state]);

  const expandRow = record => {
    return <ExpandedRowList data={timeHashList[record.key]} />;
  };

  return (
    <TableContainer>
      <Table columns={columns} dataSource={data} expandedRowRender={expandRow} />
    </TableContainer>
  );
}

export default TimeTable;
