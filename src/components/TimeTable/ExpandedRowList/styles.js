import styled from 'styled-components';
import colors from '~/styles/colors';
import { CoffeeOutlined as IconBase } from '@ant-design/icons';

export const RowBox = styled.div`
  margin-left: 7%;
  position: relative;
`;

export const CoffeeIcon = styled(IconBase)`
  color: ${colors.blue};
  position: absolute;
  left: 0;
  bottom: 35%;
`;
