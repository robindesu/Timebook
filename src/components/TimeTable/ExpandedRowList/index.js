import React from 'react';
import { List } from 'antd';
import { RowBox, CoffeeIcon } from './styles';

function ExpandedRowList({ data }) {
  return (
    <List
      size="small"
      dataSource={data}
      renderItem={item => (
        <RowBox>
          <List.Item>
            {item.isLunch && <CoffeeIcon />}
            {item.init_time} - {item.end_time} = {item.amount}
          </List.Item>
        </RowBox>
      )}
    />
  );
}

export default ExpandedRowList;
