import React from 'react';
import constants from '~/constants';

export const columns = [
  {
    title: 'Date',
    dataIndex: 'date',
    key: 'date',
  },
  {
    title: 'First Arrive',
    dataIndex: 'init_time',
    key: 'init_time',
  },
  {
    title: 'Last Exit',
    dataIndex: 'end_time',
    key: 'end_time',
  },
  {
    title: 'Lunch Amount',
    dataIndex: 'lunch_amount',
    key: 'lunch_amount',
    render: value => <span>{value.toString()}</span>,
  },
  {
    title: 'Worked Amount/ Expected',
    dataIndex: 'amount',
    key: 'amount',
    render: value => (
      <span>
        {value} / {constants.ESPECTED_TIME}
      </span>
    ),
  },
];
