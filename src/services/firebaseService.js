import { firebaseDatabase } from '~/utils/firebaseUtils';

export default class FirebaseService {
  static getData = (path, callback) => {
    firebaseDatabase
      .collection(path)
      .get()
      .then(dataSnapshot => {
        let list = [];
        dataSnapshot.forEach(doc => {
          let item = doc.data();
          item = { ...item, id: doc.id };
          list.push(item);
        });
        callback(list);
      });
  };
  static sendTime = (time_entry, callback) => {
    firebaseDatabase
      .collection('timebook')
      .add(time_entry)
      .then(doc => {
        callback(doc);
      })
      .catch(function (error) {
        console.error('Error saving time: ', error);
      });
  };
}
